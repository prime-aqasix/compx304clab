#include "Spaceship.h"

int id;

typedef struct BattleSim_struct *BattleSim;

typedef struct BattleSim_struct {
	int id;
	Spaceship s1;
	Spaceship s2;
	int max_rounds;
} BattleSim_struct;

int get_id();
int init_battle_sim(BattleSim sim, int max_rounds, char* s1_name, char* s2_name);

void start_battle(BattleSim sim);

void free_battle_sim(BattleSim self);