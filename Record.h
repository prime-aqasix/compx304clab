#include "BattleSim.h"

typedef struct Record_struct *Record;

typedef struct Record_struct {
	int id;
	BattleSim bs;
	int initial_hp_s1;
	int initial_hp_s2;
	int outcome;
	int rounds_lasted;
} Record_struct;

void record_init(Record self) {

}