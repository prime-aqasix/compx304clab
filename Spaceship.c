/**
* @author Panos Patros
**/

#include "Spaceship.h"
#include <stdio.h> /*printf*/
#include <stdlib.h> /*rand*/
#include <string.h> /*strncpy, strlen*/

#define MAX(x, y) (((x) > (y)) ? (x) : (y))

int init_spaceship(Spaceship self, char *name) {
	int name_length = strlen(name);
	self->name = malloc(name_length + 1);
	if (self->name == NULL) {
		/*Malloc failed*/
		return -1;
	}
	strncpy(self->name, name, name_length);

	self->health_points = rand() % 21 + 80;
	self->defense_points = rand() % 6 + 5;
	self->attack_points = rand() % 11 + 10;

	return 0;
}

void attack_spaceship(Spaceship self, Spaceship underAttack) {
	int attack_val = calculate_attack_power_spaceship(self);

	underAttack->health_points -= MAX(0, attack_val - underAttack->defense_points);
}

int calculate_attack_power_spaceship(Spaceship self) {
	return self->attack_points / (rand() % 2 + 1);
}

bool is_destroyed_spaceship(Spaceship self) {
	return self->health_points <= 0;
}

void print_spaceship(Spaceship self) {
	printf("%s: %d (HP), %d (AP), %d (DP)\n", self->name, self->health_points, self->attack_points, self->defense_points);
}

void free_spaceship(Spaceship self) {
	free(self->name);
	free(self);
}
