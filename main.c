/**
* @author Panos Patros
**/

#include <stdio.h>
#include <stdlib.h>
#include "BattleSim.h"

int main(){
	Spaceship s1, s2;

    srand(333134);

	BattleSim sim = (BattleSim) malloc(sizeof(BattleSim_struct));
	if(sim == NULL){
		printf("Malloc failed for Battle Simulation!\n");
		return 1;
	}

	init_battle_sim(sim, 0, "USS Enterprise","A Borg Cube");

	start_battle(sim);

	free_battle_sim(sim);

	return 0;
}
