#include "BattleSim.h"
#include <stdlib.h>
#include <stdio.h>

// I probably don't even need to initialize this
int id_generator = 0;

int get_id() {
	id++;
	return id - 1;
}

/// `max_rounds`: Maximum number of rounds to run, 0 or less means no maximum
int init_battle_sim(BattleSim sim, int max_rounds, char* s1_name, char* s2_name) {

	sim->id = get_id();

	sim->max_rounds = max_rounds;

	sim->s1 = (Spaceship) malloc(sizeof(Spaceship_struct));
	if(sim->s1 == NULL){
		printf("Malloc failed for s1!\n");
		return 1;
	}

	sim->s2 = (Spaceship) malloc(sizeof(Spaceship_struct));
	if(sim->s2 == NULL){
		printf("Malloc failed for s2!\n");
		return 1;
	}

	init_spaceship(sim->s1, s1_name);
	init_spaceship(sim->s2, s2_name);

	return 0;
}

void start_battle(BattleSim sim) {
	printf("------------------\n");
	printf("\nBattle simulation %d started with ships: \n\n", sim->id);
	print_spaceship(sim->s1);
	print_spaceship(sim->s2);

	int rounds = sim->max_rounds;
	while (--rounds) {
		Spaceship a,b;
		if (rounds % 2) {
		    a = sim->s1;
		    b = sim->s2;
		} else {
			b = sim->s1;
			a = sim->s2;
		}

		attack_spaceship(a,b);
		if (is_destroyed_spaceship(b)) {
			break;
		}
	}

	printf("\nBattle simulation %d concluded, results: \n\n", sim->id);
	print_spaceship(sim->s1);
	print_spaceship(sim->s2);

	printf("\n------------------\n");
}

int battle_results(BattleSim sim) {
	return sim->s1->health_points <= 0 ? 2 : sim->s2->health_points <= 0 ? 1 : 0;
}

void free_battle_sim(BattleSim sim) {
	free_spaceship(sim->s1);
	free_spaceship(sim->s2);

	free(sim);
}